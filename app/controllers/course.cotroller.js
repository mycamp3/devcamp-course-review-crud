const courseModel = require("../models/course.model");
const mongoose = require("mongoose");
const createCourse = async (req,res) => {
    // B1: Thu thập dữ liệu
    const {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

// console.log((req.body));
// { reqTitle: 'R46', reqDescription: 'React & NodeJs', reqStudent:20}

    // B2: Validate dữ liệu
    if(!reqTitle) {
        return res.status(400).json({
            message: "Title ko hợp lệ"
        })
    }

    if(reqStudent < 0) {
        return res.status(400).json({
            message: "Số hs ko hợp lệ"
        })
    }

    try {
        // B3: Xl dữ liệu

        var newCourse = {
            title: reqTitle,
            description: reqDescription,
            noStudent: reqStudent
        }
        const result = await courseModel.create(newCourse);

        return res.status(201).json({
            message: "Tạo thành công",
            data: result
        })
    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
     
}

const getAllCourses = async (req,res) => {
    // B1:
    // B2:
    // B3: 
    try {
        const result = await courseModel.find();

        return res.status(200).json({
            message: "Lấy ds courses thành công",
            data: result
        })
    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const getCourseById = async (req,res) => {
    // B1: Thu thập dl
    const courseid = req.params.courseid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Courseid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await courseModel.findById(courseid);

        if(result) {
            return res.status(200).json({
                message: "Lấy thông tin course thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin course"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const updateCourseById = async (req,res) => {
    // B1: Thu thập dl
    const courseid = req.params.courseid;
    const {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Courseid ko hợp lệ"
        }) 
    }

    if(reqTitle === "") {
        return res.status(400).json({
            message: "Title ko hợp lệ"
        })
    }

    if(reqStudent < 0) {
        return res.status(400).json({
            message: "Số hs ko hợp lệ"
        })
    }

    // B3: Xl dl
    try {
        var newUpdateCourse = {};
        if (reqTitle) {
            newUpdateCourse.title = reqTitle
        }
        if (reqDescription) {
            newUpdateCourse.description = reqDescription
        }
        if (reqStudent) {
            newUpdateCourse.noStudent = reqStudent
        }

        const result = await courseModel.findByIdAndUpdate(courseid, newUpdateCourse);

        if(result) {
            return res.status(200).json({
                message: "Update thông tin course thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin course"
            })
        }

    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}


const deleteCourseById = async (req,res) => {
    // B1: Thu thập dl
    const courseid = req.params.courseid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Courseid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await courseModel.findByIdAndDelete(courseid);

        if(result) {
            return res.status(200).json({
                message: "Xóa thông tin course thành công",
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin course"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

module.exports = {
    createCourse,
    getAllCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
}